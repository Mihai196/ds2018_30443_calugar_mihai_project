package integration;

import model.Product;
import model.builder.ProductBuilder;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;
import repository.product.ProductRepository;

@RunWith(SpringRunner.class)
@DataJpaTest
public class ProductIntegrationTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private ProductRepository productRepository;

    @Test
    public void findByName(){
        Product product=new ProductBuilder().setName("Numee").setPrice(177).setSportCategory("Football").setTypeCategory("Shoes").build();
        entityManager.persist(product);
        entityManager.flush();

        Product found= productRepository.findByName(product.getName()).get(0);

        //Assert.assertTrue(product.getName().equals(found.getName()));

    }
}
