package model;

public class ActivityTypes {

    public static final String ADD_PRODUCT="ADD_PRODUCT";
    public static final String UPDATE_PRODUCT="UPDATE_PRODUCT";
    public static final String DELETE_PRODUCT="DELETE_PRODUCT";
    public static final String PDF_REPORT="PDF_REPORT";
    public static final String CSV_REPORT="CSV_REPORT";
}
