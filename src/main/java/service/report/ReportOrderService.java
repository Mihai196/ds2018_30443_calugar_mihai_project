package service.report;

import model.Product;
import model.ProductOrder;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public interface ReportOrderService {

    void generateReport(String date, String deliveryProducts, List<ProductOrder> productOrders) throws IOException;
    void generateReportSold(String date, Map<String,Long> counts) throws IOException;
}
