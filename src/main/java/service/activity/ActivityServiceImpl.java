package service.activity;

import model.Activity;
import model.User;
import model.builder.ActivityBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import repository.activity.ActivityRepository;
import repository.user.UserRepository;

import java.util.Date;
import java.util.List;

@Service
public class ActivityServiceImpl implements ActivityService{
    @Autowired
    private ActivityRepository activityRepository;

    @Autowired
    private UserRepository userRepository;

    @Override
    public Boolean addActivity(long userId, Date date, String type) {
        User user=userRepository.findById(userId).orElse(null);
        Activity activity=new ActivityBuilder().setUser(user).setDate(date).setType(type).build();
        Activity activitySaved=activityRepository.save(activity);
        return activitySaved !=null;
    }

    @Override
    public List<Activity> findByUser(long userId) {
        User user=userRepository.findById(userId).orElse(null);
        return activityRepository.findByUser(user);
    }
}
