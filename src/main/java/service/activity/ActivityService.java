package service.activity;

import model.Activity;

import java.util.Date;
import java.util.List;

public interface ActivityService {

    Boolean addActivity(long userId, Date date,String type);
    List<Activity> findByUser(long userId);
}
