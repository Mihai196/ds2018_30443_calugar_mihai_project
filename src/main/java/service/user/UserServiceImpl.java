package service.user;

import model.User;
import model.builder.UserBuilder;
import model.validation.Notification;
import model.validation.UserValidator;
import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import repository.user.UserRepository;

import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Optional;
import java.security.Key;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

@Service
public class UserServiceImpl implements UserService {
    private UserRepository userRepository;


    @Autowired
    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public Notification<Boolean> addUser(String username, String password, String role) {
        System.out.println("Hello from user service");
        //BCryptPasswordEncoder enc = new BCryptPasswordEncoder();
        User user = new UserBuilder()
                .setUsername(username)
                .setPassword(password)
                .setRole(role)
                .build();
        UserValidator userValidator=new UserValidator();
        boolean userValidation=userValidator.validate(user);
        Notification<Boolean> userNotification= new Notification<>();
        if(!userValidation)
        {
            userValidator.getErrors().forEach(userNotification::addError);
            userNotification.setResult(Boolean.FALSE);
        }
        else
        {
            // Create MessageDigest instance for MD5
            MessageDigest md = null;
            try {
                md = MessageDigest.getInstance("MD5");
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }
            //Add password bytes to digest
            md.update(password.getBytes());
            //Get the hash's bytes
            byte[] bytes = md.digest();
            //This bytes[] has bytes in decimal format;
            //Convert it to hexadecimal format
            StringBuilder sb = new StringBuilder();
            for(int i=0; i< bytes.length ;i++)
            {
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            //Get complete hashed password in hex format
//            generatedPassword = sb.toString();
            user.setPassword(sb.toString());
            userRepository.save(user);
            userNotification.setResult(Boolean.TRUE);
        }
        return userNotification;

    }

    @Override
    public Notification<Boolean> updateUser(Long id, String username, String password, String role) {
        Optional<User> userOptional=userRepository.findById(id);
       //BCryptPasswordEncoder enc = new BCryptPasswordEncoder();
        User user=new User();
        if (userOptional.isPresent())
        {
            user=userOptional.get();
        }
        user.setUsername(username);
        user.setPassword(password);
        user.setRole(role);
        UserValidator userValidator=new UserValidator();
        boolean userValidation=userValidator.validate(user);
        Notification<Boolean> userNotification= new Notification<>();
        if(!userValidation)
        {
            userValidator.getErrors().forEach(userNotification::addError);
            userNotification.setResult(Boolean.FALSE);
        }
        else
        {
            //user.setPassword(enc.encode(password));
            userRepository.save(user);
            userNotification.setResult(Boolean.TRUE);
        }
        return userNotification;
    }

    @Override
    public User findByUsername(String username) {
        List<User> users=userRepository.findByUsername(username);
        if (users.isEmpty())
        {
            return null;
        }
        else
        {
            return users.get(0);
        }
    }

    @Override
    public List<User> findAll() {
        List<User> users=userRepository.findAll();
        return users;
    }

    @Override
    public List<User> findByRole(String role) {
        return userRepository.findByRole(role);
    }

    @Override
    public User findById(Long id) {
        Optional<User> userOptional=userRepository.findById(id);
        return userOptional.orElse(null);
    }

    @Override
    public void deleteUser(Long id) {
        User user=new UserBuilder().setId(id).build();
        userRepository.delete(user);

    }
}
