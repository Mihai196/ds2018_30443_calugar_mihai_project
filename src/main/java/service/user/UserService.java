package service.user;

import model.User;
import model.validation.Notification;

import java.util.List;

public interface UserService {

    Notification<Boolean> addUser(String username, String password, String role);
    Notification<Boolean> updateUser(Long id, String username, String password, String role);
    User findByUsername(String username);
    List<User> findAll();
    List<User> findByRole(String role);
    User findById(Long id);
    void deleteUser(Long id);
}
