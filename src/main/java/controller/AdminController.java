package controller;

import dto.UserDTO;
import model.Activity;
import model.ProductOrder;
import model.User;
import model.validation.Notification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import service.activity.ActivityService;
import service.productOrder.ProductOrderService;
import service.user.UserService;

import java.util.List;

@RestController
@RequestMapping("api/admin")
public class AdminController {

    @Autowired
    private UserService userService;

    @Autowired
    private ProductOrderService productOrderService;

    @Autowired
    private ActivityService activityService;

    @CrossOrigin
    @RequestMapping(value ="/allUsers", method = RequestMethod.GET)
    public List<User> viewUsers(Model model) {
        return userService.findAll();
    }


    @CrossOrigin
    @RequestMapping(value = "/addUser", method = RequestMethod.POST)
    public ResponseEntity<String> addEmployee(@RequestBody UserDTO userDTO) {
        Notification<Boolean> userNotification = userService.addUser(userDTO.getUsername(), userDTO.getPassword(), userDTO.getRole());
        if (userNotification.hasErrors()) {
            return new ResponseEntity<>(userNotification.getFormattedErrors(), HttpStatus.BAD_REQUEST);
        } else {
            return new ResponseEntity<>("User was added successfully", HttpStatus.OK);
        }
    }

    @CrossOrigin
    @RequestMapping(value = "updateUser", method = RequestMethod.POST)
    public ResponseEntity<String> updateUser(@RequestBody UserDTO userDTO) {
        Notification<Boolean> userNotification = userService.updateUser(userDTO.getId(), userDTO.getUsername(), userDTO.getPassword(), userDTO.getRole());
        if (userNotification.hasErrors()) {
            return new ResponseEntity<>(userNotification.getFormattedErrors(),HttpStatus.BAD_REQUEST);
        } else {
            return new ResponseEntity<>("User was updated successfully",HttpStatus.OK);
        }

    }

    @CrossOrigin
    @RequestMapping(value =  "deleteUser", method = RequestMethod.POST)
    public ResponseEntity<String> deleteUser(@RequestBody UserDTO userDTO) {
        userService.deleteUser(userDTO.getId());
        return new ResponseEntity<>("User was deleted successfully",HttpStatus.OK);
    }

    @CrossOrigin
    @RequestMapping(value = "allEmployees",method = RequestMethod.GET)
    public List<User> allEmployees(){
        return userService.findByRole("employee");
    }

    @CrossOrigin
    @RequestMapping(value = "trackEmployee/{employeeId}",method = RequestMethod.GET)
    public List<Activity> trackEmployee(@PathVariable long employeeId){
        return activityService.findByUser(employeeId);
    }
}
