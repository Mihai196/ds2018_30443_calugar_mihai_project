package controller;

import com.google.api.client.http.HttpResponse;
import dto.ProductDTO;
import dto.ReportStockDTO;
import model.*;
import model.validation.Notification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.*;
import service.activity.ActivityService;
import service.commandOrder.CommandOrderService;
import service.product.ProductService;
import service.productOrder.ProductOrderService;
import service.report.ReportFactory;
import service.report.ReportOrderService;
import service.report.ReportService;
import service.user.UserService;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

import static org.springframework.util.ResourceUtils.getFile;

@RestController
@RequestMapping("api/employee")
public class EmployeeController {

    @Autowired
    private ProductService productService;

    @Autowired
    private ReportFactory reportFactory;

    @Autowired
    private ActivityService activityService;

    @Autowired
    private UserService userService;

    @Autowired
    private ProductOrderService productOrderService;

    @Autowired
    private CommandOrderService commandOrderService;

    @Autowired
    private ReportOrderService reportOrderService;

    @CrossOrigin
    @RequestMapping("/allProducts")
    public List<Product> viewProducts(){
        return productService.findAllProducts();
    }


    @CrossOrigin
    @RequestMapping(value="/addProduct", method = RequestMethod.POST)
    public ResponseEntity<String> addProduct(@RequestBody ProductDTO productDTO) {
        System.out.println(productDTO.getName());
        Notification<Boolean> productNotification =
                productService.addProduct(productDTO.getName(), productDTO.getSportCategory(),productDTO.getTypeCategory(), productDTO.getDescription()
                        , productDTO.getStock(), productDTO.getPrice());
        if (productNotification.hasErrors()) {
            return new ResponseEntity<>("Product could not be inserted",HttpStatus.BAD_REQUEST);
        } else {
            activityService.addActivity(1,new Date(System.currentTimeMillis()),ActivityTypes.ADD_PRODUCT);
            return new ResponseEntity<>("Product was inserted successfully",HttpStatus.OK);
        }
    }

    @CrossOrigin
    @RequestMapping(value="/updateProduct/{productId}", method = RequestMethod.PUT)
    public ResponseEntity<String> updateProduct(@RequestBody ProductDTO productDTO,@PathVariable long productId)
    {
        Notification<Boolean> productNotification=productService.updateProductDetails(
                productId,productDTO.getName(),productDTO.getSportCategory(),productDTO.getTypeCategory(),productDTO.getPrice());
        if (productNotification.hasErrors()) {
            return new ResponseEntity<>(productNotification.getFormattedErrors(),HttpStatus.BAD_REQUEST);
        } else {
            activityService.addActivity(1,new Date(System.currentTimeMillis()),ActivityTypes.UPDATE_PRODUCT);
            return new ResponseEntity<>("Product was updated successfully",HttpStatus.OK);
        }

    }

    @CrossOrigin
    @RequestMapping(value = "/deleteProduct/{productId}", method = RequestMethod.DELETE)
    public ResponseEntity<String> deleteProduct(@PathVariable long productId) {
        productService.deleteProduct(productId);
        activityService.addActivity(1,new Date(System.currentTimeMillis()),ActivityTypes.DELETE_PRODUCT);
        return new ResponseEntity<>("Product was deleted successfully",HttpStatus.OK);
    }

    @CrossOrigin
    @RequestMapping(value = "/getById/{productId}", method = RequestMethod.GET)
    public Product getById(@PathVariable long productId) {
        return productService.findById(productId);
    }


    @CrossOrigin
    @RequestMapping(value = "trackClient/{clientId}",method = RequestMethod.GET)
    public List<ProductOrder> trackClient(@PathVariable long clientId )
    {
        User user=userService.findById(clientId);
        return productOrderService.findByClient(user);
    }


    @CrossOrigin
    @RequestMapping(value ="generatePDF",method = RequestMethod.GET)
    public byte[] generatePDFReport() throws IOException {
        //System.out.println(reportStockDTO.getReportType());
        ReportService pdfReportService= reportFactory.getReportType("pdf");
        List<Product> products=productService.findAllProducts();
        List<Product> limitedStockProducts=products.stream().filter(e->e.getStock()<5).collect(Collectors.toList());
        pdfReportService.generateReport(limitedStockProducts);
        String filePath="E:/Mihaica/Faculta/An4/DS/FinalProjectDS/Report.pdf";
        File file=getFile(filePath);
        byte[] document=FileCopyUtils.copyToByteArray(file);
        activityService.addActivity(1,new Date(System.currentTimeMillis()),ActivityTypes.PDF_REPORT);
        return document;
    }

    @CrossOrigin
    @RequestMapping(value ="generatePDFSold",method = RequestMethod.GET)
    public byte[] generatePDFReportMostSold() throws IOException {
        List<CommandOrder> commandOrders = commandOrderService.findAll();
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.MONTH, -3);
        Date result = cal.getTime();
        List<CommandOrder> last3Months=commandOrders.stream().filter(e->e.getExpectedArrivalDate().compareTo(result)>0).collect(Collectors.toList());
        List<ProductOrder> allProductOrders=new ArrayList<>();
        for(CommandOrder commandOrder:last3Months){
            List<ProductOrder> productOrders=productOrderService.findByCommandOrder(commandOrder);
            allProductOrders.addAll(productOrders);
        }
        Map<String, Long> hm = new HashMap<String, Long>();

        for (ProductOrder i : allProductOrders) {
            Long j = hm.get(i.getProduct().getName());
            hm.put(i.getProduct().getName(), (j == null) ? 1 : j + 1);
        }
        reportOrderService.generateReportSold(result.toString(),hm);
        String filePath="E:/Mihaica/Faculta/An4/DS/FinalProjectDS/MostSold.pdf";
        File file=getFile(filePath);
        byte[] document=FileCopyUtils.copyToByteArray(file);
        activityService.addActivity(1,new Date(System.currentTimeMillis()),ActivityTypes.PDF_REPORT);
        return document;
    }

    @CrossOrigin
    @RequestMapping(value = "generateCSV",method = RequestMethod.POST)
    public ResponseEntity<String> generateCSVReport(@RequestBody ReportStockDTO reportStockDTO) throws IOException {
        ReportService csvReportService= reportFactory.getReportType(reportStockDTO.getReportType());
        List<Product> products=productService.findAllProducts();
        List<Product> limitedStockProducts=products.stream().filter(e->e.getStock()<5).collect(Collectors.toList());
        csvReportService.generateReport(limitedStockProducts);
        String filePath="E:/Mihaica/Faculta/An4/DS/FinalProjectDS";
        activityService.addActivity(1,new Date(System.currentTimeMillis()),ActivityTypes.CSV_REPORT);
        return new ResponseEntity<>("A new csv report with limited stock products was created at" + filePath,HttpStatus.OK);
    }

}
