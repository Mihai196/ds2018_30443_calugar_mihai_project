package controller;


import dto.UserDTO;
import model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import service.user.UserService;

import java.util.List;

@RestController
@RequestMapping("api/user")
public class UserController {

    @Autowired
    private UserService userService;

    @CrossOrigin
    @RequestMapping(value ="/login/{username}", method = RequestMethod.GET)
    public User login(@PathVariable String username) {
        return userService.findByUsername(username);
    }
}
