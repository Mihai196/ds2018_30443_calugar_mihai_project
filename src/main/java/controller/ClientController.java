package controller;

import com.google.api.services.youtube.model.ResourceId;
import com.google.api.services.youtube.model.Thumbnail;
import dto.*;
import model.*;
import model.builder.ProductOrderBuilder;
import model.validation.Notification;
import org.apache.commons.compress.utils.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.*;
import service.commandOrder.CommandOrderService;
import service.creditCard.CreditCardService;
import service.product.ProductService;
import service.productOrder.ProductOrderService;
import service.rating.RatingService;
import service.report.ReportFactory;
import service.report.ReportOrderService;
import service.user.UserService;
import service.youtubeAPI.YoutubeService;

import com.google.api.services.youtube.model.SearchResult;

import javax.print.attribute.standard.Media;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.util.ResourceUtils.getFile;

@RestController
@RequestMapping("api/client")
public class ClientController {

    @Autowired
    private CreditCardService creditCardService;

    @Autowired
    private UserService userService;

    @Autowired
    private ProductOrderService productOrderService;

    @Autowired
    private ProductService productService;

    @Autowired
    private RatingService ratingService;

    @Autowired
    private CommandOrderService commandOrderService;

    @Autowired
    private YoutubeService youtubeService;

    @Autowired
    private ReportFactory reportFactory;

    @Autowired
    private ReportOrderService reportOrderService;

    @CrossOrigin
    @RequestMapping(value = "addCreditCard", method = RequestMethod.POST)
    public ResponseEntity<String> addCreditCard(@RequestBody CreditCardDTO creditCardDTO) {

        Notification<Boolean> creditCardNotification = creditCardService.addCreditCard(creditCardDTO.getUserId(),
                creditCardDTO.getBalance(), creditCardDTO.getBankName(), creditCardDTO.getCardNr());
        if (creditCardNotification.hasErrors()) {
            return new ResponseEntity<>(creditCardNotification.getFormattedErrors(),HttpStatus.BAD_REQUEST);
        } else {
            return new ResponseEntity<>("Credit card was added successfully",HttpStatus.OK);
        }

    }

    @CrossOrigin
    @RequestMapping(value ="allCreditCard/{userId}", method = RequestMethod.GET)
    public List<CreditCard> viewCreditCards(@PathVariable long userId) {
        return creditCardService.findByClient(userId);
    }

    @CrossOrigin
    @RequestMapping(value ="updateCreditCard", method = RequestMethod.POST)
    public ResponseEntity<String> updateBalance(@RequestBody CreditCardDTO creditCardDTO) {
        creditCardService.updateBalance(creditCardDTO.getId(),-creditCardDTO.getBalance());
        return new ResponseEntity<>("all goodie",HttpStatus.OK);
    }

    @CrossOrigin
    @RequestMapping(value ="deleteCreditCard", method = RequestMethod.POST)
    public ResponseEntity<String> deleteCreditCard(@RequestBody CreditCardDTO creditCardDTO) {
        creditCardService.deleteCreditCard(creditCardDTO.getId());
        return new ResponseEntity<>("Credit card was deleted successfully",HttpStatus.OK);
    }

    @CrossOrigin
    @RequestMapping(value = "addRating", method = RequestMethod.POST)
    public ResponseEntity<String> addRating(@RequestBody RatingDTO ratingDTO) {
        User user=userService.findById(ratingDTO.getClientId());
        Notification<Boolean> ratingNotification = ratingService.addRating(user, ratingDTO.getProductId(), ratingDTO.getValue(), ratingDTO.getDescription());
        if (ratingNotification.hasErrors()) {
            return new ResponseEntity<>(ratingNotification.getFormattedErrors(),HttpStatus.BAD_REQUEST);
        } else {
            return new ResponseEntity<>("Rating for product was added successfully",HttpStatus.OK);
        }
    }

    @CrossOrigin
    @RequestMapping(value = "myRatings/{userId}", method = RequestMethod.GET)
    public List<Rating> viewYourRatings(@PathVariable long userId) {
        User loggedUser = userService.findById(userId);
        return ratingService.findByClient(loggedUser);
    }

    @CrossOrigin
    @RequestMapping(value="ratingsProduct/{productId}",method = RequestMethod.GET)
    public List<Rating> viewRatingsProduct(@PathVariable long productId)
    {
        List<Rating> ratings=ratingService.findAllRatings();
        return ratings.stream().filter(e->e.getProduct().getId().equals(productId)).collect(Collectors.toList());
    }

    @CrossOrigin
    @RequestMapping(value="allRatings",method = RequestMethod.GET)
    public List<Rating> viewAllRatings()
    {
        return ratingService.findAllRatings();
    }

    @CrossOrigin
    @RequestMapping(value = "trackOrder/{orderId}",method = RequestMethod.GET)
    public List<OrderTrackDTO> trackYourOrder(@PathVariable long orderId)
    {
        CommandOrder commandOrder=commandOrderService.findById(orderId);
        List<ProductOrder> productOrders=productOrderService.findByCommandOrder(commandOrder);
        List<OrderTrackDTO> orderTrackDTOS=new ArrayList<>();
        for(ProductOrder productOrder:productOrders){
            OrderTrackDTO orderTrackDTO=new OrderTrackDTO(commandOrder,productOrder);
            orderTrackDTOS.add(orderTrackDTO);
        }
        return orderTrackDTOS;
    }

    @CrossOrigin
    @RequestMapping(value ="filterBySport/{sportCategory}",method = RequestMethod.GET)
    public List<Product> filterBySport(@PathVariable String sportCategory)
    {
        return productService.findBySportCategory(sportCategory);
    }

    @CrossOrigin
    @RequestMapping(value ="filterByType/{typeCategory}",method = RequestMethod.GET)
    public List<Product> filterByType(@PathVariable String typeCategory)
    {
        return productService.findByTypeCategory(typeCategory);
    }

    @CrossOrigin
    @RequestMapping(value = "searchYoutube/{productName}",method = RequestMethod.GET)
    public List<SearchYoutubeDTO> showYoutubeSearch(@PathVariable String productName)
    {
        String queryTerm=productName+" review";
        List<SearchResult> searchResults=youtubeService.getSearchResult(queryTerm);
        Iterator<SearchResult> iteratorSearchResults=searchResults.iterator();
        List<SearchYoutubeDTO> searchYoutubes=new ArrayList<>();
        while (iteratorSearchResults.hasNext()) {

            SearchResult singleVideo = iteratorSearchResults.next();
            ResourceId rId = singleVideo.getId();

            if (rId.getKind().equals("youtube#video")) {
                Thumbnail thumbnail = singleVideo.getSnippet().getThumbnails().getDefault();
                String link="https://www.youtube.com/watch?v="+rId.getVideoId();
                String title=singleVideo.getSnippet().getTitle();
                SearchYoutubeDTO searchYoutube=new SearchYoutubeDTO(link,title);
                searchYoutubes.add(searchYoutube);
            }
        }
        return searchYoutubes;
    }

    @CrossOrigin
    @RequestMapping(value = "addShopCart/{userId}/{productId}",method = RequestMethod.GET)
    public List<ProductOrder> addToShoppingCart(HttpServletRequest request,@PathVariable long userId,@PathVariable long productId)
    {
        HttpSession session = request.getSession();
        User loggedUser = userService.findById(userId);
        Product product = productService.findById(productId);
        ProductOrder productOrder = new ProductOrderBuilder().setProduct(product).setClient(loggedUser).setQuantity(1).build();
        List<ProductOrder> productOrders = (List<ProductOrder>) session.getAttribute("cartList");
        boolean additionProductOrder=false;
        if (productOrders == null) {
            productOrders = new ArrayList<>();
        }
        if(product.getStock()>2) {
            productOrders.add(productOrder);
            additionProductOrder=true;
        }
        session.setAttribute("cartList", productOrders);
        return productOrders;
//        if(additionProductOrder) {
//            String message="Product was added successfully to shopping cart";
//            return new ResponseEntity<>(message, HttpStatus.OK);
//        }
//        else {
//            String message="Product could not be added successfully to shopping cart";
//            return new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
//        }
    }

    private double computeTotalAmount(List<ProductOrder> productOrders){
        double totalAmountToBePaid = 0;
        for(ProductOrder productOrder:productOrders)
        {
            totalAmountToBePaid += productOrder.getQuantity() * productOrder.getProduct().getPrice();
        }
        return totalAmountToBePaid;
    }

    @CrossOrigin
    @RequestMapping(value="viewShopCart" ,method = RequestMethod.GET)
    public List<ProductOrder> viewShoppingCart(HttpServletRequest request){
        HttpSession session = request.getSession();
        List<ProductOrder> productOrders = (ArrayList<ProductOrder>) session.getAttribute("cartList");
        return productOrders;
    }

    @CrossOrigin
    @RequestMapping(value ="finalizeCommand/{creditCardId}", method = RequestMethod.POST)
    public CommandOrder finalizeCommand(HttpServletRequest request,@PathVariable long creditCardId,@RequestBody List<ProductOrderDTO> productOrderDTOS) {

        List<ProductOrder> productOrders = new ArrayList<>();
        for(ProductOrderDTO productOrderDTO:productOrderDTOS){
            User user=userService.findById(productOrderDTO.getClientId());
            Product product=productService.findById(productOrderDTO.getProductId());
            ProductOrder productOrder= new ProductOrderBuilder().setQuantity(1).setClient(user).setProduct(product).build();
            productOrders.add(productOrder);
        }

        double totalAmount=computeTotalAmount(productOrders);
        CreditCard creditCard=creditCardService.findById(creditCardId);
        Notification<Boolean> creditCardNotification = creditCardService.updateBalance(creditCard.getId(), totalAmount);
        if(creditCardNotification.hasErrors()){
            return null;
        }
        Notification<CommandOrder> commandOrderNotification =
                commandOrderService.addCommandOrder(new java.sql.Date(System.currentTimeMillis()+2*86400000), "FANCourier");
        CommandOrder commandOrder=commandOrderNotification.getResult();
        String errors="";
        errors=creditCardNotification.getFormattedErrors()+
                commandOrderNotification.getFormattedErrors();
        for (ProductOrder productOrder : productOrders) {
            Notification<Boolean> orderNotification = productOrderService.addProductOrder(productOrder.getProduct().getId(),
                    productOrder.getClient().getId(), commandOrder.getId(),
                    productOrder.getQuantity());
            Notification<Boolean> productNotification = productService.updateProductStock(
                    productOrder.getProduct().getId(),
                    productOrder.getProduct().getStock() - productOrder.getQuantity());
            errors+=orderNotification.getFormattedErrors()+productNotification.getFormattedErrors();
        }
        if(errors.equals("")){
            return commandOrder;
        }
        else{
            return null;
        }
    }

    @CrossOrigin
    @RequestMapping(value = "viewReceipt/{orderId}",method = RequestMethod.POST)
    public HttpEntity<byte[]> generateOrderReport(@PathVariable long orderId) throws IOException {
        CommandOrder commandOrder=commandOrderService.findById(orderId);
        List<ProductOrder> productOrders=productOrderService.findByCommandOrder(commandOrder);
        String date=" Your command is scheduled to be delivered at " +commandOrder.getExpectedArrivalDate();
        String deliveryProducts=" Delivered by " +commandOrder.getDeliveryCompany() +" You can see your products below: ";
        reportOrderService.generateReport(date,deliveryProducts,productOrders);
        String filePath="E:/Mihaica/Faculta/An3/Sem2/SD/final-project-Mihai196/FinalProject/ReportOrder.pdf";
        File file=getFile(filePath);
        byte[] document=FileCopyUtils.copyToByteArray(file);
        HttpHeaders header = new HttpHeaders();
        header.setContentType(new MediaType("application", "pdf"));
        header.set("Content-Disposition", "inline; filename=" + "ReportOrder");
        header.setContentLength(document.length);
        return new HttpEntity<byte[]>(document, header);
    }
}
