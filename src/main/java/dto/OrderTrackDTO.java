package dto;

import model.CommandOrder;
import model.ProductOrder;

import java.util.List;

public class OrderTrackDTO {

    private CommandOrder commandOrder;
    private ProductOrder productOrder;

    public OrderTrackDTO(CommandOrder commandOrder, ProductOrder productOrder) {
        this.commandOrder = commandOrder;
        this.productOrder = productOrder;
    }

    public CommandOrder getCommandOrder() {
        return commandOrder;
    }

    public void setCommandOrder(CommandOrder commandOrder) {
        this.commandOrder = commandOrder;
    }

    public ProductOrder getProductOrder() {
        return productOrder;
    }

    public void setProductOrder(ProductOrder productOrder) {
        this.productOrder = productOrder;
    }
}
