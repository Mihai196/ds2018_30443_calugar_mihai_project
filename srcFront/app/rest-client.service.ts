import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';

const endpoint = 'http://localhost:8080/api/';
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})

export class RestClientService {

  constructor(private http: HttpClient) { }

  private extractData(res: Response) {
    let body = res;
    return body || { };
  }

  getCreditCards(id) : Observable<any>{
    return this.http.get(endpoint + 'client/allCreditCard/'+id).pipe(
      map(this.extractData));
  }

  getProducts(): Observable<any> {
    return this.http.get(endpoint + 'employee/allProducts').pipe(
      map(this.extractData));
  }

  addToShoppingCart(userId,productId): Observable<any>{
    return this.http.get(endpoint + 'client/addShopCart/'+userId+'/'+productId).pipe(
      map(this.extractData));
  }

  getShoppingCartItems(): Observable<any>{
    return this.http.get(endpoint + 'client/viewShopCart/').pipe(
      map(this.extractData));
  }

  finalizeCommand(shoppingCart,creditCardId): Observable<any>{
    return this.http.post(endpoint+ "client/finalizeCommand/"+creditCardId,JSON.stringify(shoppingCart),httpOptions).pipe(
      map(this.extractData));
  }

  searchYoutube(productName): Observable<any>{
    return this.http.get(endpoint + 'client/searchYoutube/'+productName).pipe(
      map(this.extractData));
  }

  trackOrder(id): Observable<any>{
    return this.http.get(endpoint + 'client/trackOrder/'+id).pipe(
      map(this.extractData));
  }

  addCreditCard (creditCard): Observable<any> {
    console.log(creditCard);
    return this.http.post<any>(endpoint + 'client/addCreditCard', JSON.stringify(creditCard), httpOptions).pipe(
      tap((data) => console.log(data)),
      catchError(this.handleError<any>('deleteProduct'))
    );
  }

  addRating(rating): Observable<any>{
    return this.http.post<any>(endpoint + 'client/addRating', JSON.stringify(rating), httpOptions).pipe(
      tap((data) => console.log(data)),
      catchError(this.handleError<any>('addRating'))
    );
  }

  getRatings():Observable<any> {
    return this.http.get(endpoint + 'client/allRatings/').pipe(
      map(this.extractData));
  }

  updateCredit (creditCard): Observable<any> {
    return this.http.post(endpoint + 'client/updateCreditCard/', JSON.stringify(creditCard), httpOptions).pipe(
      tap(_ => console.log(`updated product id=${creditCard.id}`)),
      catchError(this.handleError<any>('update Credit'))
    );
  }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
  
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
  
      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);
  
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

}
