import { Component, OnInit, Input } from '@angular/core';
import { RestAdminService } from '../rest-admin.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  @Input() userInformation:any = { username:'',password:'',role:''}

  constructor(public rest:RestAdminService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
  }

  register(){
    this.userInformation.role="client";
    this.rest.register(this.userInformation).subscribe((result) => {
      this.router.navigate(['login']);
      console.log(result);
    }, (err) => {
      console.log(err);
    });
  }

}
