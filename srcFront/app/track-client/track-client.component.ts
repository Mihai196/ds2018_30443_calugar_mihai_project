import { Component, OnInit, Input } from '@angular/core';
import { RestService } from '../rest.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-track-client',
  templateUrl: './track-client.component.html',
  styleUrls: ['./track-client.component.css']
})
export class TrackClientComponent implements OnInit {

  @Input() clientData:any = { id:''}

  productOrders:any = [];

  constructor(public rest:RestService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
  }

  trackClient(id){
    this.productOrders = [];
    this.rest.getProductOrders(id).subscribe((data: {}) => {
      console.log(data);
      this.productOrders = data;
    });
  }

  backButton(){
    this.router.navigate(['employee/allProducts'])
  }

}
