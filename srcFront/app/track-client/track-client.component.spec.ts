import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrackClientComponent } from './track-client.component';

describe('TrackClientComponent', () => {
  let component: TrackClientComponent;
  let fixture: ComponentFixture<TrackClientComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrackClientComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrackClientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
