import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrackEmployeeComponent } from './track-employee.component';

describe('TrackEmployeeComponent', () => {
  let component: TrackEmployeeComponent;
  let fixture: ComponentFixture<TrackEmployeeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrackEmployeeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrackEmployeeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
