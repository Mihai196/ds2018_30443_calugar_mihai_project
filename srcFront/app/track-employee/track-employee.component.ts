import { Component, OnInit, Input } from '@angular/core';
import { RestAdminService } from '../rest-admin.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-track-employee',
  templateUrl: './track-employee.component.html',
  styleUrls: ['./track-employee.component.css']
})
export class TrackEmployeeComponent implements OnInit {

  @Input() clientData:any = { id:''}

  activities:any = [];

  constructor(public rest:RestAdminService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
  }

  trackEmployee(id){
    this.activities = [];
    this.rest.trackEmployee(id).subscribe((data: {}) => {
      console.log(data);
      this.activities = data;
    });
  }

  backButton(){
    this.router.navigate(['admin/allUsers'])
  }

}
