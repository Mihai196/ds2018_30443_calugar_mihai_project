import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RatingMenuComponent } from './rating-menu.component';

describe('RatingMenuComponent', () => {
  let component: RatingMenuComponent;
  let fixture: ComponentFixture<RatingMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RatingMenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RatingMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
