import { Component, OnInit, Input } from '@angular/core';
import { RestClientService } from '../rest-client.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-rating-menu',
  templateUrl: './rating-menu.component.html',
  styleUrls: ['./rating-menu.component.css']
})
export class RatingMenuComponent implements OnInit {

  @Input() ratingData = { productId:'', clientId:'',value:'',description:'' };
  ratings:any=[];
  constructor(public rest:RestClientService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.getRatings();
  }

  getRatings(){
    this.ratings = [];
    this.rest.getRatings().subscribe((data: {}) => {
      console.log(data);
      this.ratings = data;
    });
  }

  addRating(){
    const user:any =JSON.parse(sessionStorage.getItem("loggedUser"));
    this.ratingData.clientId=user.id;
    this.rest.addRating(this.ratingData).subscribe((result) => {
      this.getRatings();
      // console.log(result);
    }, (err) => {
      console.log(err);
    });
  }

  backButton(){
    this.router.navigate(['client/clientMenu'])
  }

}
