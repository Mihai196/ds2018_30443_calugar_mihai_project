import { Component, OnInit, Input } from '@angular/core';
import { RestClientService } from '../rest-client.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-credit-card-add',
  templateUrl: './credit-card-add.component.html',
  styleUrls: ['./credit-card-add.component.css']
})
export class CreditCardAddComponent implements OnInit {

  @Input() creditCardData = { balance:'', bankName:'',cardNr:'',userId:'' };

  constructor(public rest:RestClientService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
  }

  addCreditCard() {
    const user:any =JSON.parse(sessionStorage.getItem("loggedUser"));
    this.creditCardData.userId=user.id;
    this.rest.addCreditCard(this.creditCardData).subscribe((result) => {
      this.router.navigate(['/client/creditCardMenu']);
      console.log(result);
    }, (err) => {
      console.log(err);
    });
  }

  backButton(){
    this.router.navigate(['client/creditCardMenu']);
  }

}
