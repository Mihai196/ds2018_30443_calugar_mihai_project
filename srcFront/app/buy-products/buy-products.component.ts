import { Component, OnInit, Input } from '@angular/core';
import { RestClientService } from '../rest-client.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-buy-products',
  templateUrl: './buy-products.component.html',
  styleUrls: ['./buy-products.component.css']
})
export class BuyProductsComponent implements OnInit {

  @Input() creditCardData = { id:0 }
  @Input() awb = { id:0 }
  constructor(public rest:RestClientService, private route: ActivatedRoute, private router: Router) { }
  
  productss:any = [];
  shopCartProducts:any = [];
  creditCards:any = [];

  ngOnInit() {
    const user:any =JSON.parse(sessionStorage.getItem("loggedUser"));
    if(user!=null&&user.role==="client"){
      this.getProducts();
      this.getCreditCards(user.id);
      if (sessionStorage.getItem("shoppingCart"))
      {
        this.shopCartProducts=JSON.parse(sessionStorage.getItem("shoppingCart"));
      }
      else{
        this.shopCartProducts=[];
      }
    }
    else{
      this.router.navigate(['login']);
    }
  }

  getProducts() {
    this.productss = [];
    this.rest.getProducts().subscribe((data: {}) => {
      console.log(data);
      this.productss = data;
    });
  }

  addShoppingCart(productId,productName,productPrice){
    const user:any =JSON.parse(sessionStorage.getItem("loggedUser"));
    var productAdded=JSON.stringify({'clientId':user.id,'productId':productId,'productName':productName,'productPrice':productPrice});
    var productAddedJSON=JSON.parse(productAdded);
    this.shopCartProducts.push(productAddedJSON);
    sessionStorage.setItem("shoppingCart",JSON.stringify(this.shopCartProducts));
  }

  deleteFromShopCart(productId){
    sessionStorage.removeItem("shoppingCart");
    this.shopCartProducts=this.shopCartProducts.filter(item=>item.productId!==productId);
    console.log(this.shopCartProducts);
    sessionStorage.setItem("shoppingCart",JSON.stringify(this.shopCartProducts)); 
  }

  finalizeCommand(creditCardId){
    console.log(creditCardId);
    this.rest.finalizeCommand(this.shopCartProducts,creditCardId).subscribe((data: any) => {
      console.log(data);
      if(data.id!=null){
        alert('AWB is '+data.id);
        this.awb.id=data.id;
        this.shopCartProducts=[];
        sessionStorage.removeItem("shoppingCart");
      }
      else{
        alert("You don't have enough money on the account");
      }
    });
  }

  getShoppingCartItems() {
    this.shopCartProducts = [];
    this.rest.getShoppingCartItems().subscribe((data: {}) => {
      console.log(data);
      this.shopCartProducts = data;
    });
  }

  backButton(){
    this.router.navigate(['client/clientMenu']);
  }

  getCreditCards(id) {
    this.creditCards = [];
    this.rest.getCreditCards(id).subscribe((data: {}) => {
      console.log(data);
      this.creditCards = data;
    });
  }

}
