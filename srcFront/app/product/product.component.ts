import { Component, OnInit } from '@angular/core';
import { RestService } from '../rest.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {

  productss:any = [];
  urlfile:any="";

  constructor(public rest:RestService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    const user:any =JSON.parse(sessionStorage.getItem("loggedUser"));
    if(user!=null&&user.role==="employee"){
      this.getProducts();
    }
    else{
      this.router.navigate(['login']);
    }
  }

  getProducts() {
    this.productss = [];
    this.rest.getProducts().subscribe((data: {}) => {
      console.log(data);
      this.productss = data;
    });
  }

  add() {
    this.router.navigate(['/employee/addProduct']);
  }

  edit() {
    this.router.navigate(['/employee/editProduct']);
  }

  trackClient() {
    this.router.navigate(['/employee/trackClient']);
  }

  delete(id) {
    this.rest.deleteProduct(id)
      .subscribe(res => {
          this.getProducts();
        }, (err) => {
          console.log(err);
        }
      );
  }

  generatePDF(){
    this.rest.getPDF().subscribe((response)=>{
      console.log("generate",response);
      let file = new Blob([response], { type: 'application/pdf' });     
      console.log('file',file);
      var fileURL = URL.createObjectURL(file);
      console.log("url",fileURL);
      this.urlfile=fileURL;
      window.location.href=fileURL;
    })
  }

  generatePDFSold(){
    this.rest.getPDFMostSold().subscribe((response)=>{
      console.log("generate",response);
      let file = new Blob([response], { type: 'application/pdf' });     
      console.log('file',file);
      var fileURL = URL.createObjectURL(file);
      console.log("url",fileURL);
      this.urlfile=fileURL;
      window.location.href=fileURL;
    })
  }

  logout(){
    sessionStorage.removeItem("shoppingCart");
    sessionStorage.removeItem("loggedUser");
    this.router.navigate(['/login']);
  }


}