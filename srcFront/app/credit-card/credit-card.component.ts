import { Component, OnInit, Input } from '@angular/core';
import { RestClientService } from '../rest-client.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-credit-card',
  templateUrl: './credit-card.component.html',
  styleUrls: ['./credit-card.component.css']
})
export class CreditCardComponent implements OnInit {

  creditCards :any=[];

  @Input() creditCardData:any = { id:'',balance:''}

  constructor(public rest:RestClientService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    const user:any =JSON.parse(sessionStorage.getItem("loggedUser"));
    if(user!=null&&user.role==="client"){
      this.getCreditCards(user.id);
    }
    else{
      this.router.navigate(['login']);
    }
  }

  getCreditCards(id) {
    this.creditCards = [];
    this.rest.getCreditCards(id).subscribe((data: {}) => {
      console.log(data);
      this.creditCards = data;
    });
  }

  backButton(){
    this.router.navigate(['client/clientMenu']);
  }

  add(){
    this.router.navigate(['client/addCreditCard'])
  }

  edit(id){
    this.creditCardData.id=id;
    console.log(this.creditCardData);
  }

  updateBalance(){
    const user:any =JSON.parse(sessionStorage.getItem("loggedUser"));
    this.rest.updateCredit(this.creditCardData).subscribe((result) => {
      this.getCreditCards(user.id);
      console.log(result);
    }, (err) => {
      console.log(err);
    });
  }

}
