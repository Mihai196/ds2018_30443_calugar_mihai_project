import { Component, OnInit } from '@angular/core';
import { RestClientService } from '../rest-client.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-youtube-search',
  templateUrl: './youtube-search.component.html',
  styleUrls: ['./youtube-search.component.css']
})
export class YoutubeSearchComponent implements OnInit {

  constructor(public rest:RestClientService, private route: ActivatedRoute, private router: Router) { }

  youtubeSearches:any=[];

  ngOnInit() {
    this.rest.searchYoutube(this.route.snapshot.params['name']).subscribe((data: {}) => {
      console.log(data);
      this.youtubeSearches = data;
    });
  }

  backButton(){
    this.router.navigate(['client/buyProducts'])
  }
  

}
