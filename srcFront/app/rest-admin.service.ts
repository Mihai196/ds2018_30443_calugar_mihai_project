import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';

const endpoint = 'http://localhost:8080/api/';
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})
export class RestAdminService {

  constructor(private http: HttpClient) { }

  private extractData(res: Response) {
    let body = res;
    return body || { };
  }

  getUsers(): Observable<any> {
    return this.http.get(endpoint + 'admin/allUsers').pipe(
      map(this.extractData));
  }

  trackEmployee(id):Observable<any>{
    return this.http.get(endpoint + 'admin/trackEmployee/'+id).pipe(
      map(this.extractData));
  }

  login(username):Observable<any>{
    return this.http.get(endpoint + 'user/login/'+username).pipe(
      map(this.extractData));
  }

  register(user):Observable<any>{
    console.log(user);
    return this.http.post<any>(endpoint + 'admin/addUser', JSON.stringify(user), httpOptions).pipe(
      tap((product) => console.log(product)),
      catchError(this.handleError<any>('Add product'))
    );
  }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
  
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
  
      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);
  
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

}
