import { Component, OnInit, Input } from '@angular/core';
import { RestAdminService } from '../rest-admin.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-admin-user',
  templateUrl: './admin-user.component.html',
  styleUrls: ['./admin-user.component.css']
})
export class AdminUserComponent implements OnInit {

  @Input() userInformation:any = { username:'',password:'',role:''}
  users:any = [];
  constructor(public rest:RestAdminService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    const user:any =JSON.parse(sessionStorage.getItem("loggedUser"));
    if(user!=null&&user.role==="administrator"){
      this.getUsers();
    }
    else{
      this.router.navigate(['login']);
    }
  }

  getUsers() {
    this.users = [];
    this.rest.getUsers().subscribe((data: {}) => {
      console.log(data);
      this.users = data;
    });
  }

  trackEmployee(){
    this.router.navigate(['admin/trackEmployee']);
  }

  logout(){
    sessionStorage.removeItem("shoppingCart");
    sessionStorage.removeItem("loggedUser");
    this.router.navigate(['/login']);
  }

  addUser(){
    this.userInformation.role="employee";
    this.rest.register(this.userInformation).subscribe((result) => {
      this.getUsers();
      console.log(result);
    }, (err) => {
      console.log(err);
    });
  }

}
