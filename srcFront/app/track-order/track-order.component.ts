import { Component, OnInit, Input } from '@angular/core';
import { RestClientService } from '../rest-client.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-track-order',
  templateUrl: './track-order.component.html',
  styleUrls: ['./track-order.component.css']
})
export class TrackOrderComponent implements OnInit {

  @Input() orderData:any = { id:''}

  activities:any = [];

  constructor(public rest:RestClientService, private route: ActivatedRoute, private router: Router) { }

  trackOrder(id){
    this.activities = [];
    this.rest.trackOrder(id).subscribe((data: {}) => {
      console.log(data);
      this.activities = data;
    });
  }

  backButton(){
    this.router.navigate(['client/clientMenu']);
  }

  ngOnInit() {
  }

}
