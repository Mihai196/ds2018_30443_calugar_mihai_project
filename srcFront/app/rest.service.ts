import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';

const endpoint = 'http://localhost:8080/api/';
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})
export class RestService {

  constructor(private http: HttpClient) { }

  private extractData(res: Response) {
    let body = res;
    return body || { };
  }

  getProducts(): Observable<any> {
    return this.http.get(endpoint + 'employee/allProducts').pipe(
      map(this.extractData));
  }

  getProductOrders(id) : Observable<any>{
    return this.http.get(endpoint + 'employee/trackClient/'+id).pipe(
      map(this.extractData));
  }

  addProduct (product): Observable<any> {
    console.log(product);
    return this.http.post<any>(endpoint + 'employee/addProduct', JSON.stringify(product), httpOptions).pipe(
      tap((product) => console.log(product)),
      catchError(this.handleError<any>('Add product'))
    );
  }

  deleteProduct (id): Observable<any> {
    return this.http.delete<any>(endpoint + 'employee/deleteProduct/' + id, httpOptions).pipe(
      tap(_ => console.log(`deleted product id=${id}`)),
      catchError(this.handleError<any>('deleteProduct'))
    );
  }

  getProduct(id): Observable<any> {
    return this.http.get(endpoint + 'employee/getById/' + id).pipe(
      map(this.extractData));
  }

  updateProduct (id, product): Observable<any> {
    return this.http.put(endpoint + 'employee/updateProduct/' + id, JSON.stringify(product), httpOptions).pipe(
      tap(_ => console.log(`updated product id=${id}`)),
      catchError(this.handleError<any>('updateProduct'))
    );
  }

  getPDF(){
    const httpOptions = {
      // 'responseType'  : 'arraybuffer' as 'json'
      'responseType'  : 'blob' as 'json'        //This also worked
    };
    return this.http.get<any>(endpoint + 'employee/generatePDF', httpOptions);
  }

  getPDFMostSold(){
    const httpOptions = {
      // 'responseType'  : 'arraybuffer' as 'json'
      'responseType'  : 'blob' as 'json'        //This also worked
    };
    return this.http.get<any>(endpoint + 'employee/generatePDFSold', httpOptions);
  }
  
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
  
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
  
      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);
  
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
