import { Component, OnInit, Input } from '@angular/core';
import { RestAdminService } from '../rest-admin.service';
import { ActivatedRoute, Router } from '@angular/router';
import {Md5} from 'ts-md5/dist/md5';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  @Input() userInformation:any = { username:'',password:''}

  loggedUser:any ;
  constructor(public rest:RestAdminService, private route: ActivatedRoute, private router: Router) { }

  login(username) {
    this.loggedUser = {};
    // this.userInformation.password="parola123!";
    let e = Md5.hashStr(this.userInformation.password);
    console.log("Hashed",e);
    this.rest.login(username).subscribe((data: any) => {
      console.log(data);
      this.loggedUser = data;
      sessionStorage.setItem("loggedUser",JSON.stringify(this.loggedUser));
      if(this.loggedUser.role==="administrator"&&this.loggedUser.password===e)
      {
        this.router.navigate(['admin/allUsers'])
      }
      else
      if(this.loggedUser.role==="employee"&&this.loggedUser.password===e)
      {
        this.router.navigate(['employee/allProducts']);
      }
      else
      if(this.loggedUser.role==="client"&&this.loggedUser.password===e){
        this.router.navigate(['client/clientMenu']);
      }
      else{
        alert("Invalid email or password. If you don't have an account please create one.")
      }
    });
  }

  ngOnInit() {
  }

  goToRegister(){
    this.router.navigate(['register']);
  }


}
