import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { ProductComponent } from './product/product.component';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { ProductAddComponent } from './product-add/product-add.component';
import { ProductEditComponent } from './product-edit/product-edit.component';
import { TrackClientComponent } from './track-client/track-client.component';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { ClientMenuComponent } from './client-menu/client-menu.component';
import { AdminUserComponent } from './admin-user/admin-user.component';
import { TrackEmployeeComponent } from './track-employee/track-employee.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { BuyProductsComponent } from './buy-products/buy-products.component';
import { CreditCardComponent } from './credit-card/credit-card.component';
import { CreditCardAddComponent } from './credit-card-add/credit-card-add.component';
import { YoutubeSearchComponent } from './youtube-search/youtube-search.component';
import { TrackOrderComponent } from './track-order/track-order.component';
import { RatingMenuComponent } from './rating-menu/rating-menu.component';

const appRoutes: Routes = [
  {
    path: 'employee/allProducts',
    component: ProductComponent,
    data: { title: 'Product List' }
  },
  {
    path: 'employee/addProduct',
    component: ProductAddComponent,
    data: { title: 'Product Add' }
  },
  {
    path: 'employee/editProduct/:id',
    component: ProductEditComponent,
    data: { title: 'Product Edit' }
  },
  {
    path: 'employee/trackClient',
    component: TrackClientComponent,
    data: { title: 'Track Client' }
  },
  {
    path: 'admin/allUsers',
    component: AdminUserComponent,
    data: { title: 'Admin Menu' }
  },
  {
    path: 'admin/trackEmployee',
    component: TrackEmployeeComponent,
    data: { title: 'Track Employee' }
  },
  {
    path: 'login',
    component: LoginComponent,
    data: { title: 'Login' }
  },
  {
    path: '',
    component: LoginComponent,
    data: { title: 'Login' }
  },
  {
    path: 'client/clientMenu',
    component: ClientMenuComponent,
    data: { title: 'Client Menu' }
  },
  {
    path: 'client/creditCardMenu',
    component: CreditCardComponent,
    data: { title: 'Credit Card Menu' }
  },
  {
    path: 'client/addCreditCard',
    component: CreditCardAddComponent,
    data: { title: 'Credit Card Add' }
  },
  {
    path: 'client/buyProducts',
    component: BuyProductsComponent,
    data: { title: 'Buy Products' }
  },
  {
    path:'client/searchYoutube/:name',
    component:YoutubeSearchComponent,
    data:{ title: 'Search Youtube'}
  },
  {
    path:'client/trackOrder',
    component:TrackOrderComponent,
    data:{ title: 'Track Order'}
  },
  {
    path:'client/ratingMenu',
    component:RatingMenuComponent,
    data:{ title: 'Rating Menu'}
  },
  {
    path:'register',
    component:RegisterComponent,
    data:{ title: 'Register'}
  }
];

@NgModule({
  declarations: [
    AppComponent,
    ProductComponent,
    ProductAddComponent,
    ProductEditComponent,
    TrackClientComponent,
    ClientMenuComponent,
    AdminUserComponent,
    TrackEmployeeComponent,
    LoginComponent,
    RegisterComponent,
    BuyProductsComponent,
    CreditCardComponent,
    CreditCardAddComponent,
    YoutubeSearchComponent,
    TrackOrderComponent,
    RatingMenuComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    AppRoutingModule,
    RouterModule.forRoot(appRoutes),
    PdfViewerModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
