import { Component, OnInit } from '@angular/core';
import { RestService } from '../rest.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-client-menu',
  templateUrl: './client-menu.component.html',
  styleUrls: ['./client-menu.component.css']
})
export class ClientMenuComponent implements OnInit {

  constructor(public rest:RestService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    if(sessionStorage.getItem("shoppingCart")!=null)
    {
      alert("You still have items in your shopping cart.Make sure you finalize your command! :) ");
    } 
  }

  buyProducts(){
    this.router.navigate(['client/buyProducts']);
  }
  
  creditCardMenu(){
    this.router.navigate(['client/creditCardMenu'])
  }

  trackOrder(){
    this.router.navigate(['client/trackOrder']);
  }

  ratingMenu(){
    this.router.navigate(['client/ratingMenu']);
  }

  logout(){
    sessionStorage.removeItem("shoppingCart");
    sessionStorage.removeItem("loggedUser");
    this.router.navigate(['/login']);
  }

}
